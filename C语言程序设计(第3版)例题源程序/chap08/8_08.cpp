/* 【例8-8】二分查找。设已有一个10个元素的整形数组a，且按值从小到大有序。
输入一个整数x，然后在数组中查找x，如果找到，输出相应的下标，否则，输出"Not Found"。 */

/* 二分查找 */
# include<stdio.h>
int Bsearch(int *p, int n, int x);      /* 函数声明 */
int main(void)   
{
int a[10] = {1,2,3,4,5,6,7,8,9,10};     /* 有序数组 */
    int x, m;

    printf("Enter x：");            /* 提示输入x */
    scanf("%d",&x);                
m = Bsearch(a, 10, x);
    if(m >= 0)   
        printf("Index is %d \n",m);
    else 
        printf( "Not Found\n");
		
    return 0;
}
int Bsearch(int *p, int n, int x)   /* 二分查找函数 */
{   
int low, high, mid;
low = 0; high = n - 1;       /* 开始时查找区间为整个数组 */
    while (low <= high)  {          /* 循环条件 */
        mid = (low + high) / 2;     /* 中间位置 */
        if (x == p[mid])
            break;                  /* 查找成功，中止循环 */
        else if (x < p[mid])
            high = mid - 1;         /* 前半段，high前移 */ 
        else       
            low = mid + 1;          /* 后半段，low后移 */
    }    
    if(low <= high)   
        return mid;                 /* 找到返回下标 */
    else 
        return -1;                  /* 找不到返回-1 */
}

