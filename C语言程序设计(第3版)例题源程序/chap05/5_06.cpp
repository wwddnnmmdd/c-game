/*  【例5-6】分别输入两个复数的实部与虚部，计算两个复数之和与之积。 */
#include<stdio.h> 
float result_real, result_imag;			/*  全局变量，用于存放函数结果  */
	/*  函数声明  */
void complex_prod(float real1, float imag1, float real2, float imag2);
void complex_add(float real1, float imag1, float real2, float imag2);
int main(void) 
{ 
	float imag1, imag2, real1, real2;		/* 两个复数的实、虚部变量 */
	
	printf("Enter 1st complex number(real and imaginary): ");	
	scanf("%f%f", &real1, &imag1); 			/* 输入第一个复数 */
	printf("Enter 2nd complex number(real and imaginary): ");	
	scanf("%f%f", &real2, &imag2); 			/* 输入第二个复数 */
	complex_add(real1, imag1, real2, imag2);	/* 求复数之和 */
	printf("addition of complex is %f+%fi\n", result_real, result_imag);
	complex_prod(real1, imag1, real2, imag2); 	/* 求复数之积 */
	printf("product of complex is %f+%fi\n", result_real, result_imag);
	
	return 0;
}

	/* 定义求复数之和函数 */
void complex_add(float real1, float imag1, float real2, float imag2)
{
	result_real = real1 + real2;
	result_imag = imag1 + imag2;
}

	/* 定义求复数之积函数 */
void complex_prod(float real1, float imag1, float real2, float imag2)
{
	result_real = real1*real2 - imag1*imag2;
	result_imag = real1*imag2 + real2*imag1;
}
